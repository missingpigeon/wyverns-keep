#ifndef OVERVIEWWINDOW_H
#define OVERVIEWWINDOW_H

#include <QWidget>

namespace Ui {
class OverviewWindow;
}

class OverviewWindow : public QWidget
{
    Q_OBJECT

public:
    explicit OverviewWindow(QWidget *parent = 0);
    ~OverviewWindow();

private:
    Ui::OverviewWindow *ui;
};

#endif // OVERVIEWWINDOW_H
