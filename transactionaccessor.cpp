#include "transactionaccessor.h"

TransactionAccessor::TransactionAccessor()
{
    CTL = QSqlDatabase::database();
    dataModel = new QSqlQueryModel;
    getCirculation();
}

QSqlQueryModel* TransactionAccessor::getCurrentModel() {
    dataModel->setQuery(currentQuery);
    return dataModel;
}

QSqlQueryModel* TransactionAccessor::getCirculation() {
    currentQuery = "SELECT T.transactionID AS 'Transaction ID', B.title AS 'Book borrowed', "
                        "R.readerID AS 'Reader', U.username AS 'Signed Librarian', T.borrowdate AS 'Date borrowed', "
                        "T.due AS 'Due', T.returndate AS 'Date returned', T.tstatus AS 'Status', T.remarks AS 'remarks' "
                   "FROM TRANSACTIONS T, BOOKS B, READERS R, USERS U WHERE T.bookID = B.bookID AND "
                   "T.readerID = R.readerID AND T.userID = U.accountID", CTL;
    dataModel->setQuery(currentQuery);
    return dataModel;
}

QSqlQueryModel* TransactionAccessor::search(QString criterium, QString match) {
    currentQuery = "SELECT T.transactionID AS 'Transaction ID', B.title AS 'Book borrowed', "
                        "R.readerID AS 'Reader', U.username AS 'Signed Librarian', T.borroweddate AS 'Date borrowed', "
                        "T.due AS 'Due', T.returndate AS 'Date returned', T.tstatus AS 'Status', T.remarks AS 'remarks' "
                   "FROM TRANSACTIONS T, BOOKS B, READERS R, USERS U WHERE T.bookID = B.bookID AND "
                   "T.readerID = R.readerID AND T.userID = U.accountID WHERE " + criterium + " LIKE '%" + match + "%'", CTL;
    dataModel->setQuery(currentQuery);
    return dataModel;
}

Transaction* TransactionAccessor::getTransaction(QString ID) {
    QSqlQuery getTransactionQuery;
    getTransactionQuery.exec("SELECT * FROM READERS WHERE "
                    "readerID = '" + ID +"'");
    getTransactionQuery.first();
    Transaction* returnValue = new Transaction;
    returnValue->setID(getTransactionQuery.value(0).toString());
    returnValue->setReader(getTransactionQuery.value(1).toString());
    returnValue->setBook(getTransactionQuery.value(2).toString());
    returnValue->setUser(getTransactionQuery.value(3).toString());
    returnValue->setBorrowDate(getTransactionQuery.value(4).toDate());
    returnValue->setDue(getTransactionQuery.value(5).toDate());
    returnValue->setReturnDate(getTransactionQuery.value(6).toDate());
    returnValue->setStatus(getTransactionQuery.value(7).toString());
    returnValue->setRemarks(getTransactionQuery.value(7).toString());
    return returnValue;
}

void TransactionAccessor::addTransaction(Transaction* newTransaction) {
    QSqlQuery addTransactionQuery;
    addTransactionQuery.exec("INSERT INTO TRANSACTIONS (transactionID, readerID, bookID, "
                      "userID, borrowdate, due, returndate, tstatus, remarks) VALUES ('"
                      + newTransaction->getID() + "', '" + newTransaction->getReader()
                      + "', '" + newTransaction->getBook()
                      + "', '" + newTransaction->getUser()
                      + "', '" + newTransaction->getBorrowDate().toString("yyyy-MM-dd")
                      + "', '" + newTransaction->getDue().toString("yyyy-MM-dd")
                      + "', '" + newTransaction->getReturnDate().toString("yyyy-MM-dd")
                      + "', '" + newTransaction->getStatus()
                      + "', '" + newTransaction->getRemarks() + "')");
}

void TransactionAccessor::deleteTransaction(Transaction* TransactionToDelete) {
    QSqlQuery deleteTransactionQuery;
    deleteTransactionQuery.exec("DELETE FROM TRANSACTIONS WHERE "
                      "transactionID = '" + TransactionToDelete->getID()
                      + "'");
}

void TransactionAccessor::updateTransaction(Transaction* TransactionToUpdate) {
    QSqlQuery updateTransactionQuery;
    updateTransactionQuery.exec("UPDATE TRANSACTIONS SET transactionID = '"
                         + TransactionToUpdate->getID() + "', readerID = '" + TransactionToUpdate->getReader()
                         + "', bookID = '" + TransactionToUpdate->getBook()
                         + "', userID = '" + TransactionToUpdate->getUser()
                         + "', borrowdate = '" + TransactionToUpdate->getBorrowDate().toString("yyyy-MM-dd")
                         + "', due = '" + TransactionToUpdate->getDue().toString("yyyy-MM-dd")
                         + "', returndate = '" + TransactionToUpdate->getReturnDate().toString("yyyy-MM-dd")
                         + "', tstatus = '" + TransactionToUpdate->getStatus()
                         + "', remarks = '" + TransactionToUpdate->getRemarks()
                         + "' WHERE TransactionID = '" + TransactionToUpdate->getID() + "'");
}
