#include "databasewindow.h"
#include "ui_databasewindow.h"

DatabaseWindow::DatabaseWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DatabaseWindow)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Dialog);
}

DatabaseWindow::~DatabaseWindow()
{
    delete ui;
}

void DatabaseWindow::on_pushButton_2_clicked()
{
    this->close();
}

void DatabaseWindow::on_pushButton_clicked()
{
    switch (ui->comboBox->currentIndex()) {
    case (CMB_MYSQL) :
        db = QSqlDatabase::addDatabase("QMYSQL");
        break;
    case (CMB_SQLSERVER) :
        db = QSqlDatabase::addDatabase("QODBC");
        break;
    default:
        break;
    }
    // We use one default connection across the application, so
    // there's no need to specify a connection name
    // The ability to use user-specified database names might
    // be added in a later version. For now, this will do.
    db.setDatabaseName("WYVERN");
    db.setHostName(ui->txtHostname->text());
    db.setPort(ui->txtPort->text().toInt());
    db.setUserName(ui->txtUsername->text());
    db.setPassword(ui->txtPassword->text());
    bool ok = db.open();
    emit connectDatabase(ok);
    this->close();
}
