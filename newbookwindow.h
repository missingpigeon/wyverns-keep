#ifndef NEWBOOKWINDOW_H
#define NEWBOOKWINDOW_H

#include <QWidget>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include "bookaccessor.h"

namespace Ui {
class NewBookWindow;
}

class NewBookWindow : public QWidget
{
    Q_OBJECT

public:
    explicit NewBookWindow(QWidget *parent = 0);
    ~NewBookWindow();

private slots:
    void on_btnCancelSummary_clicked();

    void on_btnSummary_clicked();

    void on_btnCancel_clicked();

    void on_btnAdd_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_btnApplySummary_clicked();

private:
    Ui::NewBookWindow *ui;
    BookAccessor* databaseAccess;
};

#endif // NEWBOOKWINDOW_H
