QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WyvernsKeep
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    overviewwindow.cpp \
    cataloguewindow.cpp \
    loginwindow.cpp \
    circulationwindow.cpp \
    databasewindow.cpp \
    readersform.cpp \
    settingswindow.cpp \
    book.cpp \
    bookaccessor.cpp \
    newbookwindow.cpp \
    editbookwindow.cpp \
    transaction.cpp \
    reader.cpp \
    readeraccessor.cpp \
    newreaderwindow.cpp \
    editreaderwindow.cpp \
    transactionaccessor.cpp \
    newtransactionwindow.cpp

HEADERS  += mainwindow.h \
    overviewwindow.h \
    cataloguewindow.h \
    loginwindow.h \
    circulationwindow.h \
    databasewindow.h \
    readersform.h \
    settingswindow.h \
    book.h \
    bookaccessor.h \
    newbookwindow.h \
    editbookwindow.h \
    transaction.h \
    reader.h \
    readeraccessor.h \
    newreaderwindow.h \
    editreaderwindow.h \
    transactionaccessor.h \
    newtransactionwindow.h

FORMS    += mainwindow.ui \
    overviewwindow.ui \
    cataloguewindow.ui \
    loginwindow.ui \
    circulationwindow.ui \
    databasewindow.ui \
    readersform.ui \
    settingswindow.ui \
    newbookwindow.ui \
    editbookwindow.ui \
    newreaderwindow.ui \
    editreaderwindow.ui \
    newtransactionwindow.ui

RESOURCES += \
    wyvern_resources.qrc
