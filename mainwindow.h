#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMdiArea>
#include <QMenu>
#include "overviewwindow.h"
#include "cataloguewindow.h"
#include "circulationwindow.h"
#include "readersform.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_tabOverview_clicked();

    void on_tabCatalogue_clicked();

    void on_tabCirculation_clicked();

    void on_tabUsers_clicked();

private:
    Ui::MainWindow *ui;
    QMenu* main_menu;
    // Instancing pointers
    OverviewWindow* sub_overview;
    CatalogueWindow* sub_catalogue;
    CirculationWindow* sub_circulation;
    ReadersForm* sub_readers;
    // Locating pointers
    QMdiSubWindow* mdi_overview;
    QMdiSubWindow* mdi_catalogue;
    QMdiSubWindow* mdi_circulation;
    QMdiSubWindow* mdi_readers;
};

#endif // MAINWINDOW_H
