#ifndef READERSFORM_H
#define READERSFORM_H

#include <QWidget>
#include "newreaderwindow.h"
#include "editreaderwindow.h"

namespace Ui {
class ReadersForm;
}

class ReadersForm : public QWidget
{
    Q_OBJECT

public:
    explicit ReadersForm(QWidget *parent = 0);
    void refreshContent();
    void displayDetailedInfo();
    void displayProfilePicture();
    void recordSelected(const QModelIndex& index);
    ~ReadersForm();

private slots:
    void on_btnAdd_clicked();

    void on_btnModify_clicked();

    void on_tableView_clicked(const QModelIndex &index);

    void on_btnRefresh_clicked();

    void on_btnRemove_clicked();

    void on_btnSearch_clicked();

private:
    Ui::ReadersForm *ui;
    QSqlDatabase db;
    QSqlQueryModel* displayModel;
    ReaderAccessor* databaseAccess;
    Reader* selectedReader;
};

#endif // READERSFORM_H
