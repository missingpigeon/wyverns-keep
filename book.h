// Data transfer object for BOOKS
#ifndef BOOK_H
#define BOOK_H

#include <QWidget>

class Book {
private:
    QString ID;
    QString ISBN;
    QString Title;
    QString Type;
    QString Genre;
    QString Author;
    QString Publisher;
    int YearPublished;
    int Quantity;
    int Length;
    QString Summary;
    QString CoverArtURL;
public:
    QString getID();
    QString getISBN();
    QString getTitle();
    QString getType();
    QString getGenre();
    QString getAuthor();
    QString getPublisher();
    int getYearPublished();
    int getQuantity();
    int getLength();
    QString getSummary();
    QString getCoverArtURL();
    void setID(QString newID);
    void setISBN(QString newISBN);
    void setTitle(QString newTitle);
    void setType(QString newType);
    void setGenre(QString newGenre);
    void setAuthor(QString newAuthor);
    void setPublisher(QString newPublisher);
    void setYearPublished(int newYearPublished);
    void setQuantity(int newID);
    void setLength(int newID);
    void setSummary(QString newSummary);
    void setCoverArtURL(QString newURL);
};

#endif // BOOK_H
