#include "reader.h"

Reader::Reader()
{

}

QString Reader::getID() {
    return ID;
}

QString Reader::getFullName() {
    return fullName;
}

QDate Reader::getDOB() {
    return DOB;
}

QString Reader::getGender() {
    return gender;
}

QString Reader::getAddress() {
    return address;
}

QString Reader::getPhone() {
    return phone;
}

QString Reader::getEmail() {
    return email;
}

QString Reader::getProfilePicture(){
    return profilepicture;
}

void Reader::setID(QString new_ID) {
    ID = new_ID;
}

void Reader::setName(QString new_name) {
    fullName = new_name;
}

void Reader::setDOB(QDate new_DOB) {
    DOB = new_DOB;
}

void Reader::setGender(QString new_gender) {
    gender = new_gender;
}

void Reader::setPhone(QString new_phone) {
    phone = new_phone;
}

void Reader::setAddress(QString new_address) {
    address = new_address;
}

void Reader::setEmail(QString new_email) {
    email = new_email;
}

void Reader::setProfilePicture(QString new_pic) {
    profilepicture = new_pic;
}
