#ifndef CIRCULATIONWINDOW_H
#define CIRCULATIONWINDOW_H

#include <QWidget>
#include "newtransactionwindow.h"

namespace Ui {
class CirculationWindow;
}

class CirculationWindow : public QWidget
{
    Q_OBJECT

public:
    explicit CirculationWindow(QWidget *parent = 0);
    ~CirculationWindow();
    void refreshContent();

private slots:
    void on_btnRefresh_clicked();

    void recordSelected(const QModelIndex& index);

    void on_btnStatus_clicked();

    void on_btnIntact_clicked();

    void on_btnDamaged_clicked();

    void on_btnLost_clicked();

    void on_pushButton_7_clicked();

    void on_btnAdd_clicked();

private:
    Ui::CirculationWindow *ui;
    QSqlDatabase db;
    QSqlQueryModel* displayModel;
    TransactionAccessor* databaseAccess;
    Transaction* selectedTransaction;
};

#endif // CIRCULATIONWINDOW_H
