#include "Book.h"

QString Book::getID() {
    return ID;
}

QString Book::getISBN() {
    return ISBN;
}

QString Book::getTitle() {
    return Title;
}

QString Book::getType() {
    return Type;
}

QString Book::getGenre() {
    return Genre;
}

QString Book::getAuthor() {
    return Author;
}

QString Book::getPublisher() {
    return Publisher;
}

int Book:: getYearPublished() {
    return YearPublished;
}

int Book:: getQuantity() {
    return Quantity;
}

int Book:: getLength() {
    return Length;
}

QString Book::getSummary() {
    return Summary;
}

QString Book::getCoverArtURL() {
    return CoverArtURL;
}

void Book:: setID(QString newID) {
    ID = newID;
}

void Book:: setISBN(QString newISBN) {
    ISBN = newISBN;
}

void Book:: setTitle(QString newTitle) {
    Title = newTitle;
}

void Book:: setType(QString newType) {
    Type = newType;
}

void Book:: setGenre(QString newGenre) {
    Genre = newGenre;
}

void Book:: setAuthor(QString newAuthor) {
    Author = newAuthor;
}

void Book:: setPublisher(QString newPublisher) {
    Publisher = newPublisher;
}

void Book:: setYearPublished(int NewYearPublished) {
    YearPublished = NewYearPublished;
}

void Book:: setQuantity(int newQuantity) {
    Quantity = newQuantity;
}

void Book:: setLength(int newLength) {
    Length = newLength;
}

void Book:: setSummary(QString newSummary) {
    Summary = newSummary;
}

void Book:: setCoverArtURL(QString newURL) {
    CoverArtURL = newURL;
}
