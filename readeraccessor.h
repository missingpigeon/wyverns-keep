#ifndef READERACCESSOR_H
#define READERACCESSOR_H

#include <QWidget>
#include <QSql>
#include <QDebug>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlQueryModel>
#include "reader.h"

#define RDSRCH_ID "readerID";
#define RDSRCH_FULLNAME "fullname";
#define RDSRCH_DOB "DOB";
#define RDSRCH_GENDER "gender";
#define RDSRCH_ADDRESS "address";
#define RDSRCH_PHONE "address";
#define RDSRCH_EMAIL "email";

class ReaderAccessor
{
private:
    QSqlDatabase CTL;
    QSqlQueryModel* dataModel;
    QString currentQuery;
public:
    ReaderAccessor();
    QSqlQueryModel* getCurrentModel();
    QSqlQueryModel* getReaderList();
    QSqlQueryModel* search(QString criterium, QString match);
    Reader* getReader(QString ID);
    void addReader(Reader* newReader);
    void deleteReader(Reader* ReaderToDelete);
    void updateReader(Reader* ReaderToUpdate);
};

#endif // READERACCESSOR_H
