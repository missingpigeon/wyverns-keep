#ifndef CATALOGUEWINDOW_H
#define CATALOGUEWINDOW_H

#include <QWidget>
#include <QSql>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlTableModel>
#include <QMessageBox>
#include <QDebug>
#include "bookaccessor.h"
#include "newbookwindow.h"
#include "editbookwindow.h"

namespace Ui {
class CatalogueWindow;
}

class CatalogueWindow : public QWidget
{
    Q_OBJECT

public:
    explicit CatalogueWindow(QWidget *parent = 0);
    ~CatalogueWindow();
    void refreshContent();
    void displayPreviewInfo();
    void displayDetailedInfo();
    void displayCoverArt();

private slots:
    void on_btnViewList_clicked();

    void on_btnViewIcons_clicked();

    void on_btnFilterAll_clicked();

    void recordSelected(const QModelIndex& index);

    void on_btnAdd_clicked();

    void on_btnRefresh_clicked();

    void on_btnModify_clicked();

    void on_btnCloseDetails_clicked();

    void on_btnViewSelected_clicked();

    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_btnRemove_clicked();

    void on_btnFilterBooks_clicked();

    void on_btnFilterMagazines_clicked();

    void on_btnFilterNewspapers_clicked();

    void on_btnSearch_clicked();

private:
    Ui::CatalogueWindow *ui;
    QSqlDatabase db;
    QSqlQueryModel* displayModel;
    BookAccessor* databaseAccess;
    Book* selectedBook;
};

#endif // CATALOGUEWINDOW_H
