#ifndef TRANSACTION_H
#define TRANSACTION_H

#include <QWidget>
#include <QDate>

class Transaction
{
private:
    QString ID;
    QString readerID;
    QString bookID;
    QString userID;
    QDate borrowDate;
    QDate due;
    QDate returnDate;
    QString status;
    QString remarks;
public:
    QString getID();
    QString getReader();
    QString getBook();
    QDate getBorrowDate();
    QDate getReturnDate();
    QString getStatus();
    QString getRemarks();
    QString getUser();
    QDate getDue();
    void setID(QString new_ID);
    void setReader(QString new_reader);
    void setBook(QString new_book);
    void setBorrowDate(QDate new_bdate);
    void setReturnDate(QDate new_rdate);
    void setStatus(QString new_status);
    void setRemarks(QString new_remarks);
    void setUser(QString new_user);
    void setDue(QDate new_due);
    Transaction();
};

#endif // TRANSACTION_H
