#include "editbookwindow.h"
#include "ui_editbookwindow.h"

EditBookWindow::EditBookWindow(QWidget *parent, QString editID) :
    QWidget(parent),
    ui(new Ui::EditBookWindow)
{
    ui->setupUi(this);
    databaseAccess = new BookAccessor;
    ID = editID;
    ui->stackedWidget->setCurrentIndex(0);
}

void EditBookWindow::showEvent(QShowEvent *event) {
    fillInformation(ID);
}

EditBookWindow::~EditBookWindow()
{
    delete ui;
}

void EditBookWindow::on_btnCancelSummary_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Warning!", "If you go back right now, "
                                                    "your changes will be discarded.\n"
                                                    "Are you sure you want to go back?",
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        ui->txtSummaryPreview->setText("...");
        ui->txtSummary->clear();
        ui->stackedWidget->setCurrentIndex(0);
    } else {}
}

void EditBookWindow::on_btnSummary_clicked()
{
        ui->stackedWidget->setCurrentIndex(1);
}

void EditBookWindow::on_btnUpdate_clicked()
{
    Book* toBeAdded = new Book;
    toBeAdded->setID(ui->txtID->text());
    toBeAdded->setISBN(ui->txtISBN->text());
    toBeAdded->setTitle(ui->txtTitle->text());
    toBeAdded->setType(ui->cmbType->currentText());
    toBeAdded->setGenre(ui->txtGenre->text());
    toBeAdded->setAuthor(ui->txtAuthor->text());
    toBeAdded->setPublisher(ui->txtPublisher->text());
    toBeAdded->setYearPublished(ui->txtYearPublished->text().toInt());
    toBeAdded->setLength(ui->txtLength->text().toInt());
    toBeAdded->setQuantity(ui->txtQuantity->text().toInt());
    toBeAdded->setSummary(ui->txtSummary->toPlainText());
    toBeAdded->setCoverArtURL(ui->txtCoverURL->toPlainText());
    databaseAccess->updateBook(toBeAdded);
    this->close();
}

void EditBookWindow::on_btnBrowse_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select Image File",
    QDir::homePath());
       if(!fileName.isEmpty())
       {
           QImage image(fileName);
           if(image.isNull())
           {
               ui->txtCoverURL->setPlainText("Failed to display image");
           }
           QGraphicsScene* scene = new QGraphicsScene();
           QPixmap* coverPreview = new QPixmap(QPixmap::fromImage(image));
           scene->addPixmap(*coverPreview);
           ui->pctCover->setScene(scene);
           ui->pctCover->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
           ui->txtCoverURL->setPlainText(fileName);
       }
}

void EditBookWindow::on_btnClear_clicked()
{
    ui->txtCoverURL->clear();
    ui->pctCover->scene()->clear();
    ui->pctCover->update();
}

void EditBookWindow::on_btnApplySummary_clicked()
{
    ui->txtSummaryPreview->setText(ui->txtSummary->toPlainText());
    ui->stackedWidget->setCurrentIndex(0);
}

void EditBookWindow::on_btnCancel_clicked()
{
    this->close();
}

void EditBookWindow::fillInformation(QString ID) {
    // Get selected book
    Book* selectedBook;
    selectedBook = databaseAccess->getBook(ID);
    // Fill detail forms with information from selected book
    ui->txtID->setText(selectedBook->getID());
    ui->txtISBN->setText(selectedBook->getISBN());
    ui->txtTitle->setText(selectedBook->getTitle());
    ui->cmbType->setCurrentText(selectedBook->getType());
    ui->txtGenre->setText(selectedBook->getGenre());
    ui->txtAuthor->setText(selectedBook->getAuthor());
    ui->txtPublisher->setText(selectedBook->getPublisher());
    ui->txtYearPublished->setText(QString::number(selectedBook->getYearPublished()));
    ui->txtLength->setText(QString::number(selectedBook->getLength()));
    ui->txtQuantity->setText(QString::number(selectedBook->getQuantity()));
    ui->txtSummary->setPlainText(selectedBook->getSummary());
    ui->txtSummaryPreview->setText(selectedBook->getSummary());
    ui->txtCoverURL->setPlainText(selectedBook->getCoverArtURL());
    QImage image(selectedBook->getCoverArtURL());
    QGraphicsScene* scene = new QGraphicsScene();
    QPixmap* coverPreview = new QPixmap(QPixmap::fromImage(image));
    scene->addPixmap(*coverPreview);
    ui->pctCover->setScene(scene);
    ui->pctCover->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
}


