#include "newbookwindow.h"
#include "ui_newbookwindow.h"

NewBookWindow::NewBookWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NewBookWindow)
{
    ui->setupUi(this);
    databaseAccess = new BookAccessor;
    ui->stackedWidget->setCurrentIndex(0);
}

NewBookWindow::~NewBookWindow()
{
    delete ui;
}

void NewBookWindow::on_btnCancelSummary_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Warning!", "If you go back right now, "
                                                    "your changes will be discarded.\n"
                                                    "Are you sure you want to go back?",
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        ui->txtSummaryPreview->setText("...");
        ui->txtSummary->clear();
        ui->stackedWidget->setCurrentIndex(0);
    } else {}
}

void NewBookWindow::on_btnSummary_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void NewBookWindow::on_btnCancel_clicked()
{
    this->close();
}

void NewBookWindow::on_btnAdd_clicked()
{
    Book* toBeAdded = new Book;
    toBeAdded->setID(ui->txtID->text());
    toBeAdded->setISBN(ui->txtISBN->text());
    toBeAdded->setTitle(ui->txtTitle->text());
    toBeAdded->setType(ui->cmbType->currentText());
    toBeAdded->setGenre(ui->txtGenre->text());
    toBeAdded->setAuthor(ui->txtAuthor->text());
    toBeAdded->setPublisher(ui->txtPublisher->text());
    toBeAdded->setYearPublished(ui->txtYearPublished->text().toInt());
    toBeAdded->setLength(ui->txtLength->text().toInt());
    toBeAdded->setQuantity(ui->txtQuantity->text().toInt());
    toBeAdded->setSummary(ui->txtSummary->toPlainText());
    toBeAdded->setCoverArtURL(ui->txtCoverURL->toPlainText());
    databaseAccess->addBook(toBeAdded);
    this->close();
}

void NewBookWindow::on_pushButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select Image File",
    QDir::homePath());
       if(!fileName.isEmpty())
       {
           QImage image(fileName);
           if(image.isNull())
           {
               ui->txtCoverURL->setPlainText("Failed to display image");
           }
           QGraphicsScene* scene = new QGraphicsScene();
           QPixmap* coverPreview = new QPixmap(QPixmap::fromImage(image));
           scene->addPixmap(*coverPreview);
           ui->pctCover->setScene(scene);
           ui->pctCover->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
           ui->txtCoverURL->setPlainText(fileName);
       }
}

void NewBookWindow::on_pushButton_2_clicked()
{
    ui->txtCoverURL->clear();
    ui->pctCover->scene()->clear();
    ui->pctCover->update();
}

void NewBookWindow::on_btnApplySummary_clicked()
{
    ui->txtSummaryPreview->setText(ui->txtSummary->toPlainText());
    ui->stackedWidget->setCurrentIndex(0);
}
