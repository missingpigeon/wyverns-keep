#include "newtransactionwindow.h"
#include "ui_newtransactionwindow.h"

NewTransactionWindow::NewTransactionWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NewTransactionWindow)
{
    ui->setupUi(this);
}

NewTransactionWindow::~NewTransactionWindow()
{
    delete ui;
}

void NewTransactionWindow::on_btnAdd_clicked()
{
    Transaction* toBeAdded = new Transaction;
    toBeAdded->setID(ui->txtID->text());
    toBeAdded->setReader(ui->txtReader->text());
    toBeAdded->setBook(ui->txtBook->text());
    toBeAdded->setUser(ui->txtUser->text());
    toBeAdded->setBorrowDate(ui->deBorrowDate->date());
    toBeAdded->setDue(ui->deDue->date());
    toBeAdded->setReturnDate(ui->deReturnDate->date());
    toBeAdded->setStatus(ui->cmbStatus->currentText());
    toBeAdded->setRemarks(ui->txtRemarks->toPlainText());
    databaseAccess->addTransaction(toBeAdded);
    this->close();
}

void NewTransactionWindow::on_btnCancel_clicked()
{
    this->close();
}
