#include "overviewwindow.h"
#include "ui_overviewwindow.h"

OverviewWindow::OverviewWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OverviewWindow)
{
    ui->setupUi(this);
}

OverviewWindow::~OverviewWindow()
{
    delete ui;
}
