#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    sub_overview = NULL;
    sub_catalogue = NULL;
    sub_circulation = NULL;
    sub_readers = NULL;
    mdi_overview = NULL;
    mdi_catalogue = NULL;
    mdi_circulation = NULL;
    mdi_readers = NULL;

    on_tabOverview_clicked();

    main_menu = new QMenu(this);
    main_menu->addAction(ui->actionLogout);
    main_menu->addSeparator();
    main_menu->addAction(ui->actionPreferences);
    main_menu->addAction(ui->actionHelp);
    ui->btnSystem->setMenu(main_menu);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_tabOverview_clicked()
{
    if (mdi_overview == NULL) {
        sub_overview = new OverviewWindow;
        mdi_overview = ui->mdiArea->addSubWindow(sub_overview, Qt::FramelessWindowHint);
        sub_overview->showMaximized();
        sub_overview->activateWindow();
    }
    else ui->mdiArea->setActiveSubWindow(mdi_overview);
}

void MainWindow::on_tabCatalogue_clicked()
{
    if (mdi_catalogue == NULL) {
        sub_catalogue = new CatalogueWindow;
        mdi_catalogue = ui->mdiArea->addSubWindow(sub_catalogue, Qt::FramelessWindowHint);
        sub_catalogue->showMaximized();
        sub_catalogue->activateWindow();
    }
    else ui->mdiArea->setActiveSubWindow(mdi_catalogue);
}

void MainWindow::on_tabCirculation_clicked()
{
    if (mdi_circulation == NULL) {
        sub_circulation = new CirculationWindow;
        mdi_circulation = ui->mdiArea->addSubWindow(sub_circulation, Qt::FramelessWindowHint);
        sub_circulation->showMaximized();
        sub_circulation->activateWindow();
    }
    else ui->mdiArea->setActiveSubWindow(mdi_circulation);
}

void MainWindow::on_tabUsers_clicked()
{
    if (mdi_readers == NULL) {
        sub_readers = new ReadersForm;
        mdi_readers = ui->mdiArea->addSubWindow(sub_readers, Qt::FramelessWindowHint);
        sub_readers->showMaximized();
        sub_readers->activateWindow();
    }
    else ui->mdiArea->setActiveSubWindow(mdi_readers);
}
