#ifndef EDITBOOKWINDOW_H
#define EDITBOOKWINDOW_H

#include <QWidget>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include "bookaccessor.h"

namespace Ui {
class EditBookWindow;
}

class EditBookWindow : public QWidget
{
    Q_OBJECT

public:
    explicit EditBookWindow(QWidget *parent = 0, QString editID = "");
    ~EditBookWindow();
    void showEvent(QShowEvent *event);
    void fillInformation(QString ID);

private slots:
    void on_btnCancelSummary_clicked();

    void on_btnSummary_clicked();

    void on_btnUpdate_clicked();

    void on_btnBrowse_clicked();

    void on_btnClear_clicked();

    void on_btnApplySummary_clicked();

    void on_btnCancel_clicked();

private:
    Ui::EditBookWindow *ui;
    QString ID;
    BookAccessor* databaseAccess;
};

#endif // EDITBOOKWINDOW_H
