#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QWidget>
#include <QSql>
#include <QtSql/QSqlQuery>
#include <QMessageBox>
#include <QDebug>
#include "databasewindow.h"

namespace Ui {
class LoginWindow;
}

class LoginWindow : public QWidget
{
    Q_OBJECT

public:
    explicit LoginWindow(QWidget *parent = 0);
    ~LoginWindow();

private slots:
    void on_toolButton_2_clicked();

    void on_toolButton_3_clicked();

    void on_toolButton_clicked();

    void update_indicator(bool status);

private:
    Ui::LoginWindow *ui;
    DatabaseWindow* dataconfig;
    bool connected;
};

#endif // LOGINWINDOW_H
