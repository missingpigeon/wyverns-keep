#include "cataloguewindow.h"
#include "ui_cataloguewindow.h"

CatalogueWindow::CatalogueWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CatalogueWindow)
{
    ui->setupUi(this);
    connect(ui->tableView, SIGNAL(clicked(const QModelIndex&)),
            this, SLOT(recordSelected(const QModelIndex&)));
    ui->splitter->setStretchFactor(0, 1);
    ui->splitter->setStretchFactor(1, 5);
    ui->splitter->setStretchFactor(2, 1);
    databaseAccess = new BookAccessor;
    displayModel = new QSqlQueryModel;
    on_btnFilterAll_clicked();
    ui->btnViewSelected->setEnabled(false);
    ui->btnRemove->setEnabled(false);
    ui->btnModify->setEnabled(false);
    ui->mainView->setCurrentIndex(0);
}

CatalogueWindow::~CatalogueWindow()
{
    delete ui;
}

void CatalogueWindow::refreshContent() {
    ui->tableView->setModel(databaseAccess->getCurrentModel());
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->show();
}

void CatalogueWindow::recordSelected(const QModelIndex &index) {
    // Get selected book
    QString ID = index.sibling(index.row(), 0).data().toString();
    selectedBook = databaseAccess->getBook(ID);
    // Display information of selected book
    displayPreviewInfo();
    displayDetailedInfo();
    displayCoverArt();
    // Enable View selected button
    ui->btnViewSelected->setEnabled(true);
    ui->btnModify->setEnabled(true);
    ui->btnRemove->setEnabled(true);
}

void CatalogueWindow::on_btnViewList_clicked()
{
    ui->listView->setViewMode(QListView::ListMode);
}

void CatalogueWindow::on_btnViewIcons_clicked()
{
    ui->listView->setViewMode(QListView::IconMode);
}

void CatalogueWindow::on_btnFilterAll_clicked()
{
    ui->tableView->setModel(databaseAccess->getCatalogue());
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->show();
}

void CatalogueWindow::on_btnAdd_clicked()
{
    NewBookWindow* NewBookChild = new NewBookWindow();
    NewBookChild->show();
}

void CatalogueWindow::on_btnRefresh_clicked()
{
    refreshContent();
}

void CatalogueWindow::on_btnModify_clicked()
{
    QModelIndexList indexes = ui->tableView->selectionModel()->selection().indexes();
    QModelIndex index = indexes.at(0);
    QString currentID = index.sibling(index.row(), 0).data().toString();
    EditBookWindow* EditBookChild = new EditBookWindow(0, currentID);
    EditBookChild->show();
}

void CatalogueWindow::on_btnCloseDetails_clicked()
{
    ui->mainView->setCurrentIndex(0);
    ui->btnViewSelected->show();
}


void CatalogueWindow::on_btnViewSelected_clicked()
{
    ui->mainView->setCurrentIndex(1);
    ui->btnViewSelected->hide();
}

void CatalogueWindow::displayPreviewInfo() {
    ui->lblTitle->setText(selectedBook->getTitle());
    ui->lblGenre->setText(selectedBook->getGenre());
    ui->lblAuthor->setText(selectedBook->getAuthor());
    ui->lblPublisher->setText(selectedBook->getPublisher());
    ui->lblYear->setText(QString::number(selectedBook->getYearPublished()));
}

void CatalogueWindow::displayDetailedInfo() {
    ui->lblDTitle->setText(selectedBook->getTitle());
    ui->lblDGenre->setText(selectedBook->getGenre());
    ui->lblDType->setText("Type: " + selectedBook->getType());
    ui->lblDAuthor->setText(selectedBook->getAuthor());
    ui->lblDPublisher->setText(selectedBook->getPublisher());
    ui->lblDISBN->setText(selectedBook->getISBN());
    ui->lblDYear->setText("Published in " + QString::number(selectedBook->getYearPublished()));
    ui->lblDStock->setText(QString::number(selectedBook->getQuantity()) + " in stock");
    ui->lblDLength->setText(QString::number(selectedBook->getLength()) + " pages");
    ui->txtDSummary->setPlainText(selectedBook->getSummary());
}

void CatalogueWindow::displayCoverArt() {
    QImage coverArt(selectedBook->getCoverArtURL());
    QGraphicsScene* scene = new QGraphicsScene();
    QPixmap* coverPixmap = new QPixmap(QPixmap::fromImage(coverArt));
    scene->addPixmap(*coverPixmap);
    ui->pctCoverPreview->setScene(scene);
    ui->pctCoverPreview->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
    ui->pctCoverPreview->show();
    ui->pctDetailsCover->setScene(scene);
    ui->pctDetailsCover->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
    ui->pctDetailsCover->show();
}

void CatalogueWindow::on_tableView_doubleClicked(const QModelIndex &index)
{
    ui->tableView->selectRow(index.row());
    recordSelected(index);
    ui->mainView->setCurrentIndex(1);
}

void CatalogueWindow::on_btnRemove_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Warning!", "This action is not reversible.\n"
                                                    "Are you sure you want to delete\n"
                                                    "the selected item(s)?",
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        QModelIndexList selection = ui->tableView->selectionModel()->selectedRows();
        for(int i=0; i< selection.count(); i++)
        {
            QModelIndex index = selection.at(i);
            QString ID = index.sibling(index.row(), 0).data().toString();
            Book* bookToDelete = databaseAccess->getBook(ID);
            databaseAccess->deleteBook(bookToDelete);
        }
        ui->btnRemove->setEnabled(false);
    }
    else {}
    refreshContent();
}

void CatalogueWindow::on_btnFilterBooks_clicked()
{
    ui->tableView->setModel(databaseAccess->getFilterBooks());
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->show();
}

void CatalogueWindow::on_btnFilterMagazines_clicked()
{
    ui->tableView->setModel(databaseAccess->getFilterMagazines());
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->show();
}

void CatalogueWindow::on_btnFilterNewspapers_clicked()
{
    ui->tableView->setModel(databaseAccess->getFilterNewspapers());
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->show();
}

void CatalogueWindow::on_btnSearch_clicked()
{
    QString criterium;
    if(ui->comboBox->currentText() == "ID") criterium = BKSRCH_ID;
    if(ui->comboBox->currentText() == "ISBN") criterium = BKSRCH_ISBN;
    if(ui->comboBox->currentText() == "Title") criterium = BKSRCH_TITLE;
    if(ui->comboBox->currentText() == "Category") criterium = BKSRCH_CATEGORY;
    if(ui->comboBox->currentText() == "Author") criterium = BKSRCH_AUTHOR;
    if(ui->comboBox->currentText() == "Publisher") criterium = BKSRCH_PUBLISHER;
    if(ui->comboBox->currentText() == "Year published") criterium = BKSRCH_YEAR;
    if(ui->comboBox->currentText() == "Stock") criterium = BKSRCH_STOCK;
    if(ui->comboBox->currentText() == "Length") criterium = BKSRCH_LENGTH;
    QString match = ui->txtSearch->text();
    databaseAccess->search(criterium, match);
}
