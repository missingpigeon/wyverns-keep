#include "loginwindow.h"
#include "mainwindow.h"
#include "ui_loginwindow.h"

LoginWindow::LoginWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoginWindow)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Dialog);
    dataconfig = NULL;
    connected = false;
}

LoginWindow::~LoginWindow()
{
    delete ui;
}

void LoginWindow::on_toolButton_2_clicked()
{
    if (connected == true) {
        QSqlDatabase db = QSqlDatabase::database();
        QString username = ui->txtUsername->text();
        QString pass = ui->txtPassword->text();
        QString loginQuery = "SELECT * FROM USERS WHERE username = '"
                + username + "' AND userpassword = '" + pass + "'";
        QSqlQuery dbQuery(db);
        dbQuery.exec(loginQuery);
        if (dbQuery.size() != 1)
        {
            QMessageBox::critical(this, "Unable to login",
                                  "Invalid username or password. Please try again!");
            return;
        }
        else {
            MainWindow* w = new MainWindow;
            w->show();
            w->activateWindow();
            this->close();
        }
    }
    else {
        QMessageBox::information(this, "No database",
                                 "No database server connection detected. You need\n"
                                 "to connect to a database "
                                 "server first before attempting to log in!\n\n");
    }
}

void LoginWindow::on_toolButton_3_clicked()
{
    qApp->exit();
}

void LoginWindow::on_toolButton_clicked()
{
    if (dataconfig == NULL) dataconfig = new DatabaseWindow;
    connect(dataconfig, &DatabaseWindow::connectDatabase,
            this, &LoginWindow::update_indicator);
    dataconfig->show();
    dataconfig->activateWindow();
}

void LoginWindow::update_indicator(bool status) {
    connected = status;
    if (status == true) {
        ui->database_indicator->setStyleSheet("background-color: #26A65B;");
    }
    else {
        ui->database_indicator->setStyleSheet("background-color: #EF4836;");
    }
}
