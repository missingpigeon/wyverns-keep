#include "readeraccessor.h"

ReaderAccessor::ReaderAccessor()
{
    CTL = QSqlDatabase::database();
    dataModel = new QSqlQueryModel;
    getReaderList();
}

QSqlQueryModel* ReaderAccessor::getCurrentModel() {
    dataModel->setQuery(currentQuery);
    return dataModel;
}

QSqlQueryModel* ReaderAccessor::getReaderList() {
    currentQuery = "SELECT readerID AS 'Reader ID', fullname AS 'Full name', "
                        "DOB AS 'Date of birth', gender AS 'Gender', address AS 'Address', "
                        "phone AS 'Phone number', email AS 'E-mail address' FROM READERS", CTL;
    dataModel->setQuery(currentQuery);
    return dataModel;
}

QSqlQueryModel* ReaderAccessor::search(QString criterium, QString match) {
    currentQuery = "SELECT readerID AS 'Reader ID', fullname AS 'Full name', "
                   "DOB AS 'Date of birth', gender AS 'Gender', address AS 'Address', "
                   "phone AS 'Phone number', email AS 'E-mail address' FROM READERS "
                   "WHERE " + criterium + " LIKE '%" + match + "%'", CTL;
    dataModel->setQuery(currentQuery);
    return dataModel;
}

Reader* ReaderAccessor::getReader(QString ID) {
    QSqlQuery getReaderQuery;
    getReaderQuery.exec("SELECT * FROM READERS WHERE "
                    "readerID = '" + ID +"'");
    getReaderQuery.first();
    Reader* returnValue = new Reader;
    returnValue->setID(getReaderQuery.value(0).toString());
    returnValue->setName(getReaderQuery.value(1).toString());
    returnValue->setDOB(getReaderQuery.value(2).toDate());
    returnValue->setGender(getReaderQuery.value(3).toString());
    returnValue->setAddress(getReaderQuery.value(4).toString());
    returnValue->setPhone(getReaderQuery.value(5).toString());
    returnValue->setEmail(getReaderQuery.value(6).toString());
    returnValue->setProfilePicture(getReaderQuery.value(7).toString());
    return returnValue;
}

void ReaderAccessor::addReader(Reader* newReader) {
    QSqlQuery addReaderQuery;
    addReaderQuery.exec("INSERT INTO READERS (readerID, fullname, DOB, "
                      "gender, address, phone, email, mug) VALUES ('"
                      + newReader->getID() + "', '" + newReader->getFullName()
                      + "', '" + newReader->getDOB().toString("yyyy-MM-dd")
                      + "', '" + newReader->getGender()
                      + "', '" + newReader->getAddress() + "', '"
                      + newReader->getPhone() + "', '" + newReader->getEmail()
                        + "', '" + newReader->getProfilePicture() + "')");
}

void ReaderAccessor::deleteReader(Reader* ReaderToDelete) {
    QSqlQuery deleteReaderQuery;
    deleteReaderQuery.exec("DELETE FROM READERS WHERE "
                      "readerID = '" + ReaderToDelete->getID()
                      + "'");
}

void ReaderAccessor::updateReader(Reader* ReaderToUpdate) {
    QSqlQuery updateReaderQuery;
    updateReaderQuery.exec("UPDATE READERS SET readerID = '"
                         + ReaderToUpdate->getID() + "', fullname = '" + ReaderToUpdate->getFullName()
                         + "', DOB = '" + ReaderToUpdate->getDOB().toString("yyyy-MM-dd")
                         + "', gender = '" + ReaderToUpdate->getGender()
                         + "', address = '" + ReaderToUpdate->getAddress()
                         + "', phone = '" + ReaderToUpdate->getPhone()
                         + "', email = '" + ReaderToUpdate->getEmail()
                         + "', mug = '" + ReaderToUpdate->getProfilePicture()
                         + "' WHERE readerID = '" + ReaderToUpdate->getID() + "'");
}
