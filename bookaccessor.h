// Data access object for BOOKS

#ifndef BOOKACCESSOR_H
#define BOOKACCESSOR_H

#include <QWidget>
#include <QSql>
#include <QDebug>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlQueryModel>
#include "book.h"

#define BKSRCH_ID "bookID";
#define BKSRCH_ISBN "ISBN";
#define BKSRCH_TITLE "title";
#define BKSRCH_CATEGORY "category";
#define BKSRCH_AUTHOR "author";
#define BKSRCH_PUBLISHER "publisher";
#define BKSRCH_YEAR "yearpublished";
#define BKSRCH_STOCK "quantity";
#define BKSRCH_LENGTH "length";

class BookAccessor
{
private:
    QSqlDatabase CTL;
    QSqlQueryModel* dataModel;
    QString currentQuery;
public:
    BookAccessor();
    QSqlQueryModel* getCurrentModel();
    QSqlQueryModel* getCatalogue();
    QSqlQueryModel* getFilterBooks();
    QSqlQueryModel* getFilterMagazines();
    QSqlQueryModel* getFilterNewspapers();
    QSqlQueryModel* search(QString criterium, QString match);
    Book* getBook(QString ID);
    void addBook(Book* newBook);
    void deleteBook(Book* BookToDelete);
    void updateBook(Book* BookToUpdate);
};

#endif // BOOKACCESSOR_H
