#include "transaction.h"

Transaction::Transaction()
{

}

QString Transaction::getID() {
    return ID;
}

QString Transaction::getReader() {
    return readerID;
}

QString Transaction::getBook() {
    return bookID;
}

QDate Transaction::getBorrowDate() {
    return borrowDate;
}

QDate Transaction::getReturnDate() {
    return returnDate;
}

QString Transaction::getStatus() {
    return status;
}

QString Transaction::getRemarks() {
    return remarks;
}

void Transaction::setID(QString new_ID) {
    ID = new_ID;
}

void Transaction::setReader(QString new_reader) {
    readerID = new_reader;
}

void Transaction::setBook(QString new_book) {
    bookID = new_book;
}

void Transaction::setBorrowDate(QDate new_bdate) {
    borrowDate = new_bdate;
}

void Transaction::setReturnDate(QDate new_rdate) {
    returnDate = new_rdate;
}

void Transaction::setStatus(QString new_status) {
    status = new_status;
}

void Transaction::setRemarks(QString new_remarks) {
    remarks = new_remarks;
}

QString Transaction::getUser() {
    return userID;
}

QDate Transaction::getDue() {
    return due;
}

void Transaction::setUser(QString new_user) {
    userID = new_user;
}

void Transaction::setDue(QDate new_due) {
    due = new_due;
}
