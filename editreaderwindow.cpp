#include "editreaderwindow.h"
#include "ui_editreaderwindow.h"

EditReaderWindow::EditReaderWindow(QWidget *parent, QString editID) :
    QWidget(parent),
    ui(new Ui::EditReaderWindow)
{
    ui->setupUi(this);
    databaseAccess = new ReaderAccessor;
    ID = editID;
}

EditReaderWindow::~EditReaderWindow()
{
    delete ui;
}

void EditReaderWindow::fillInformation(QString ID) {
    // Get selected reader
    Reader* selectedReader;
    selectedReader = databaseAccess->getReader(ID);
    // Fill detail forms with information from selected Reader
    ui->txtID->setText(selectedReader->getID());
    ui->txtName->setText(selectedReader->getFullName());
    ui->deDOB->setDate(selectedReader->getDOB());
    ui->cmbGender->setCurrentText(selectedReader->getGender());
    ui->txtAddress->setText(selectedReader->getAddress());
    ui->txtPhone->setText(selectedReader->getPhone());
    ui->txtEmail->setText(selectedReader->getEmail());
    ui->txtCoverURL->setPlainText(selectedReader->getProfilePicture());
    QImage image(selectedReader->getProfilePicture());
    QGraphicsScene* scene = new QGraphicsScene();
    QPixmap* coverPreview = new QPixmap(QPixmap::fromImage(image));
    scene->addPixmap(*coverPreview);
    ui->pctCover->setScene(scene);
    ui->pctCover->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
}

void EditReaderWindow::showEvent(QShowEvent *event) {
    fillInformation(ID);
}

void EditReaderWindow::on_btnCancel_clicked()
{
    this->close();
}

void EditReaderWindow::on_btnAddMug_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select Image File",
    QDir::homePath());
       if(!fileName.isEmpty())
       {
           QImage image(fileName);
           if(image.isNull())
           {
               ui->txtCoverURL->setPlainText("Failed to display image");
           }
           QGraphicsScene* scene = new QGraphicsScene();
           QPixmap* coverPreview = new QPixmap(QPixmap::fromImage(image));
           scene->addPixmap(*coverPreview);
           ui->pctCover->setScene(scene);
           ui->pctCover->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
           ui->txtCoverURL->setPlainText(fileName);
       }
}

void EditReaderWindow::on_pushButton_2_clicked()
{
    ui->txtCoverURL->clear();
    ui->pctCover->scene()->clear();
    ui->pctCover->update();
}

void EditReaderWindow::on_btnAdd_clicked()
{
    Reader* toBeUpdated = new Reader;
    toBeUpdated->setID(ui->txtID->text());
    toBeUpdated->setName(ui->txtName->text());
    toBeUpdated->setDOB(ui->deDOB->date());
    toBeUpdated->setGender(ui->cmbGender->currentText());
    toBeUpdated->setAddress(ui->txtAddress->text());
    toBeUpdated->setPhone(ui->txtPhone->text());
    toBeUpdated->setEmail(ui->txtEmail->text());
    toBeUpdated->setProfilePicture(ui->txtCoverURL->toPlainText());
    databaseAccess->updateReader(toBeUpdated);
    this->close();
}
