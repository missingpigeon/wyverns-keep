#ifndef EDITREADERWINDOW_H
#define EDITREADERWINDOW_H

#include <QWidget>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include "readeraccessor.h"

namespace Ui {
class EditReaderWindow;
}

class EditReaderWindow : public QWidget
{
    Q_OBJECT

public:
    explicit EditReaderWindow(QWidget *parent = 0, QString editID = "");
    void showEvent(QShowEvent *event);
    void fillInformation(QString ID);
    ~EditReaderWindow();

private slots:
    void on_btnCancel_clicked();

    void on_btnAddMug_clicked();

    void on_pushButton_2_clicked();

    void on_btnAdd_clicked();

private:
    Ui::EditReaderWindow *ui;
    QString ID;
    ReaderAccessor* databaseAccess;
};

#endif // EDITREADERWINDOW_H
